#!groovy

def slack_channel = '#general'
def isFeatureBranch = !(BRANCH_NAME == 'develop' || BRANCH_NAME == 'master')
def branch        = BRANCH_NAME
def build_id      = env.build_id

def TERRAFORM_VERSION = "0.12.24"

pipeline {
  agent any

  post {
    always {
      script {
        build_status =  currentBuild.result
        echo "build_status = " + build_status

        subject = "${build_status}: Job '${env.JOB_NAME} [Build ${env.BUILD_NUMBER}]'"
        summary = "${subject} (${env.BUILD_URL})"

        if (build_status == 'SUCCESS') {
          colorCode = '#00FF00'       // GREEN
        } else {
          colorCode = '#FF0000'       // RED
        }
        slackSend (channel: slack_channel, color: colorCode, message: summary)
      }
    }
  }

  stages {

    stage('Env Variables') {
      steps {
        sh "printenv"
      }
    }

    stage('checkout') {
      steps {
        git 'https://loveforu@bitbucket.org/loveforu/apgw_lambda.git'
      }
    }

    stage ("Feature or PR or Develop branch") {
      when { expression { BRANCH_NAME != 'master' }}
      stages {

        stage('deploy') {
          steps {
            sh '''set +x
            cd terraform
	    aws s3 cp example.zip s3://build-landing-aws-jenkins-terraform/example.zip
            terraform init
            terraform apply -auto-approve'''
          }
        }

        stage("Approval Step") {
          steps {
            input id: "ApprovalStep" , message: "Do you want to destroy Develop system?"
          }
        }

        stage('destroy') {
          steps {
            sh '''set +x
            cd terraform
            aws s3 rm s3://build-landing-aws-jenkins-terraform/example.zip
            terraform destroy -auto-approve'''
          }
        }

      }
    }

    stage("Master branch") {
      when { expression { BRANCH_NAME == 'master' }}
      stages {
        stage('deploy') {
          steps {
            sh '''set +x
            cd terraform
            aws s3api create-bucket build-landing-aws-jenkins-terraform
	    aws s3 cp example.zip s3://build-landing-aws-jenkins-terraform/example.zip
            terraform init
            terraform apply -auto-approve'''
          }
        }

        stage("Approval Step") {
          steps {
            input id: "ApprovalStep" , message: "Do you want to destroy Production system?"
          }
        }

        stage('destroy') {
          steps {
            sh '''set +x
            cd terraform
            aws s3 rm s3://build-landing-aws-jenkins-terraform/example.zip
            terraform destroy -auto-approve
            aws s3api delete-bucket build-landing-aws-jenkins-terraform'''
          }
        }
      }
    }
  }
}
